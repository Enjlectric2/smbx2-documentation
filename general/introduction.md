![A user’s guide to SMBX2 b4](images/image42.png)

# Introduction

SMBX2 is a hack of the SMBX Mario fangame engine which adds lua scripting capabilities, allowing users to make just about anything they can imagine.

The base game makes use of the new scripting features to fix old bugs, add new standard gameplay elements and provide new types of functionality such as [a text parser](/modules/textplus.md) and [3D rendering](/modules/lib3d.md).

For editing levels, SMBX2 uses the [Moondust Editor](https://wohlsoft.ru/pgewiki/Moondust_Editor), replacing the original SMBX editor and adding new features like copy-paste and undo. Some of its key components are explained [here](/general/editor.md).

If you are looking for fan-games to play in SMBX, check out the [Episodes forum](https://www.supermariobrosx.org/forums/viewforum.php?f=36) on the SMBX forums.